import "*.pp"

class runlevel {

  package {
    "sysv-rc-conf":
      ensure => installed;
  }

  # Disable all the runlevels for the service by default
  #
  # If a level string is passed in, then we will disable
  # all the runlevels, only if the levels passed in do
  # not agree with the levels configured. This prepares
  # the runlevel::enable class to set the levels to what
  # the user is expecting, by clearing out the current
  # settings if they differ (and not doing anything if
  # they dont).
  
  define disable_all ( $level = false )
  {
    case $level {
      false: {
        exec {
          "sysv-rc-conf --level 0123456S $name off":
            require => Package["sysv-rc-conf"],
        }
      }
      default: {
        exec {
          "sysv-rc-conf --level 0123456S $name off":
            unless => "[ $level -eq `sysv-rc-conf --list $name | perl -ane 's/:on// and print for @F'` ]";
        }
      }
    }
  }
  
  define disable_level ( $level = false )
  {
    case $level { false: { err("need to define a runlevel to disable!") } }
    exec {
      "sysv-rc-conf --level $level $name off":
        require => Package["sysv-rc-conf"];
    }
  }  
  
  define enable_all ()
  {
    exec {
      "sysv-rc-conf $name on":
        require => Package["sysv-rc-conf"];
    }
  }
  
  define enable_level ( $level = false )
  {
    
    case $level { false: { err("need to define a runlevel to enable!") } }
    
    # first we must clear the current setup to prepare to set the
    # levels to the ones specified. If we do not do this then when you
    # specificy the levels "34" you may be surprised to find that the
    # system actually has runlevel 2,3,4 all set because it was
    # already set to start in 2 and sysv-rc-conf just enables the
    # level you specify. In otherwords, this is not an additive
    # runlevel configuration.
    disable_all { "$name": level => "$level", before => Exec["sysv-rc-conf --level $level $name on"] }
    
    exec {
      "sysv-rc-conf --level $level $name on":
        require => Package["sysv-rc-conf"];
    }
  }  
}

